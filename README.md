# Verifiering av tjänstedomäner

***OBS! Detta är en äldre version av verifieringsscripten. Aktuell version finns på https://bitbucket.org/rivta-tools/verify-scripts/.***


* * *


Verktyg för att verifiera och hantera innehållet i en tjänstedomän. Dessa script bygger främst på regelverket i 
[RIV-TA Konfigurationsstyrning](http://rivta.se/documents/ARK_0007).

## Installera Python

De script som har extension ".py" är implementerade i programmeringsspråket Python, och kräver att Python version 2.7.X är installerad på datorn (OBS, fungerar INTE med Python version 3). 
 
I Linux och MacOs är Python normalt förinstallerat. Det kan annars laddas ner från http://www.python.org/download/.
 
Mer information om att använda Python under Windows återfinns här: http://docs.python.org/2/using/windows.html. 
 
## Ladda ned scriptet
Aktuell version av scriptet finns under taggen [release-1.2](https://bitbucket.org/rivta-tools/verifydomainfolder/src/release-1.2) i detta repository.

## Köra Pythonscript

För Unixbaserade system (inkl MacOS) bör det vara tillräckligt att göra scriptet _executable_, lägga det i pathen och sedan köra det direkt. Ex:


```
> chmod 755 verifyDomainFolder.py
> ./verifyDomainFolder.py 
```

 
I Windows får man ge det som en parameter till _python_-kommandet.
 
```
C:\User\kurre> python verifyDomainFolder.py
```


## verifyDomainFolder.py 1.2 

Detta script utgår från en _trunk/_ eller specifik _tags/tag-namn/_ mapp och går igenom mappstrukturen för en Tjänstedomän och verifierar att alla obligatoriska objekt existerar på rätt plats med rätt namn (enligt 
[http://rivta.se/documents/ARK_0007 RIV-TA Konfigurationsstyrning]). Scriptet varnar även när det upptäcks innehåll som ej är obligatoriskt. Detta kan, men behöver inte,
representera ett fel. 
 
Om scriptet verifierar en struktur under en _tag_ så kommer även namnet på taggen att verifieras gentemot namnstättningsreglerna i Konfigurationsstyrningen. 
Det sker då en mappning mot överliggande mapp-namn (upp till _riv_ i strukturen som alltså måste vara utcheckat). 
Man kan även välja att be scriptet skapa en zip-fil. Det förutsätter att det finns ett _zip-kommando_ tillgängligt i PATH-en på den dator som används. 

Scriptet lämnar en returkod som motsvarar antalet hittade fel. RC=0 innebär alltså att allt är ok, och kan nyttjas om det används automatiserat. 

Anges flaggan "-h" skrivs en användarinstruktion ut.